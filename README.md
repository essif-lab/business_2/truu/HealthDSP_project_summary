# **Healthcare Digital Staff Passport Project Summary**

For more information contact:
Simon Wickes, COO, Truu Ltd. [simon@truu.id](mailto:simon@truu.id)

## **Background**
In the UK, over 500,000 NHS patient engagements are lost every year as a result of pre-employment and identity checks for trainees. 60,000 junior doctors rotate between hospitals every six months to a year, requiring time away from clinical duties, increased administrative burden and the cost of travel. Truu’s digital passport allows doctors’ to remotely complete pre-employment checks, saving the NHS 40,000 lost clinical days and up to £200m per year on these checks. Reported failure rates for onboarding at NHS Trusts are as high as 65% due to out of date or missing documentation. This problem is not limited to the UK healthcare system. During the pandemic it took over 2 weeks to onboard physicians to US field hospitals due to the administrative requirements. In Australia, significant levels of administration and documentation evidence are required to move between state medical boards. Furthermore, it takes over 3 months for doctors to move countries due to requirement to present identification, qualification and licensing documents for evidence. 

## **About Truu**
Truu was founded in 2015 by two NHS doctors who personally experienced the inefficiences in the healthcare systems regarding pre-employment and identity checks and the effect they have one healthcare worker satisfaction and frustration and patient care. Dr Nijjar and Dr Goodier identified SSI as a structure and methodology that could be applied to this scenario, enabling healthcare workers to own their credentials and complete checks remotely. This means data is more securem, controlled by the individual and minimises administration time and time away from patient care.

Dr Nijjar has contributed to the All Party Parliamentary report on Blockchain in Healthcare and has been advising the NHS on the benefits of SSI for this use case. 

Truu’s healthcare digital staff passport platform creates trusted digital identities for healthcare workers using verifiable credentials meaning checks can be completed remotely and securely reducing onboarding time from 2 months to 2 minutes. Truu’s digital passport also enables passwordless single sign on to clinical apps and IT systems reducing delays to patient care and increasing governance. Truu is founded on the shared principles of SSI and medical ethics, namely privacy and consent

## **ESSIF Project**

Our project aims to address key parts of the doctor’s journey in private medical practice. The aim of which is to demonstrate how SSI can be used to reduce admin burden and increase governance by taking doctors through the existing paper-based process but replacing the steps with Verified Credentials. Truu has previously demonstrated the efficacy of the approach in the controlled and bounded ecoystem of the UK National Health Service. However, within the UK, EU and internationally, there are large private healthcare systems that have differing challenges. This project will develop on Evernym's Verity Architecture to create a digital staff passport relevant to private healthcare and demonstrate a flow of a private consultant onboarding at a private hospital using credentials gained from a range of regulatory, oversight and insurance providers. 

### SSI Assurance Community
The lead organisation is the General Medical Council. They are responsible for regulating doctors through accreditation and governance processes. PHIN (Private Healthcare Information Network) oversees the private healthcare industry in the UK, and together with Healthcode, who maintain a register of all private doctors and practicing privileges. Fuelled provide sessional medical insurance. Through the project, we can demonstrate how rapid sessional insurance can be provided for doctors on a per day basis. PHIN will facilitate access to a hospital. They will be able to onboard the doctor using consolidated credential proofs from the other issuers. 
